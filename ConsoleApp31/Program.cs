﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp31
{
    class Program
    {
        static void Main(string[] args)
        {
            northwindEntities ne = new northwindEntities();
            ne.Database.Log = Console.WriteLine;
            var SalesData = from x in ne.Order_Details
                            select new
                            {
                                x.Order.OrderDate,
                                x.Product.ProductName,
                                x.Order.Customer.CompanyName,
                                x.Order.Customer.Country,
                                x.Quantity,
                                x.UnitPrice
                            };

            while (true)
            {
                decimal hinnapiir = 0;
                string country = "";
                int aasta = 0;

                Console.Write("Mille järgi otsime: h-hind, c-country, a-aasta, x-lõpetame: ");
                switch ((Console.ReadLine() + " ").Substring(0, 1).ToLower())
                {
                    case "h":
                        Console.Write("Anna hinnapiir: ");
                        hinnapiir = decimal.Parse(Console.ReadLine());
                        break;
                    case "c":
                        Console.Write("Anna riik: ");
                        country = Console.ReadLine();
                        break;
                    case "a":
                        Console.Write("Milline aasta: ");
                        aasta = int.Parse(Console.ReadLine());
                        break;
                    case "x":
                        return;
                }


                foreach (var x in SalesData
                    .Where(x => x.UnitPrice * x.Quantity > hinnapiir || hinnapiir == 0)
                    .Where(x => x.Country == country || country == "")
                    .Where(x => x.OrderDate.Value.Year == aasta || aasta == 0)
                    .GroupBy(x => new { x.Country, x.OrderDate.Value.Year })
                    .Select(x => new { x.Key.Country, x.Key.Year, SalesAmount = x.Sum(y => y.UnitPrice * y.Quantity) })
                    )
                    Console.WriteLine(x);


            }
        }
    }
}
